const http = require('http')
const fs = require('fs')

const outIndex = (req, res, data) => {
  const imgProd = 'https://cdn.vuetifyjs.com/images/cards/docks.jpg'
  const imgCanary = 'https://cdn.vuetifyjs.com/images/cards/plane.jpg'
  const img = process.env.ENVIRONMENT === 'canary' ? imgCanary : imgProd
  data = data.split('%IMAGE%').join(img)
  
  data = data
    .split('%ENV%')
    .join(
      '' +
        process.env.ENVIRONMENT
    )
  // console.log(data)

  const podName = process.env.POD_NAME ? process.env.POD_NAME : 'POD_NAME'
  data = data
    .split('%POD_NAME%')
    .join(podName)

  res.writeHead(200, { 'Content-Type': 'text/html' })
  res.end(data)
}

const outText = (req, res, url) => {
  fs.readFile(url, 'utf-8', (err, data) => {
    if (!err) {
      if (url === 'public/index.html') {
        return outIndex(req, res, data)
      } else {
        if (url.endsWith('.css')) {
          res.writeHead(200, { 'Content-Type': 'text/css' })
          res.end(data)
        } else {
          res.writeHead(200, { 'Content-Type': 'application/octet-stream' })
          res.end(data)
        }
      }
    } else {
      res.writeHead(500, { 'Content-Type': 'text/html' })
      res.end('Internal Server Error')
    }
  })
}

const outBinary = (req, res, url) => {
  fs.readFile(url, (err, data) => {
    if (!err) {
      if (url.endsWith('.ico')) {
        res.writeHead(200, { 'Content-Type': 'image/x-icon' })
        res.end(data)
      } else {
        res.writeHead(200, { 'Content-Type': 'application/octet-stream' })
        res.end(data)
      }
    } else {
      res.writeHead(500, { 'Content-Type': 'text/html' })
      res.end('Internal Server Error')
    }
  })
}

const server = http.createServer(function(req, res) {
  let url = req.url
  if (url === '/' || url === '') {
    url = '/index.html'
  }
  url = 'public' + url
  console.log('Receive request on : ' + process.env.ENVIRONMENT + '.  [URL] ' + url + '  [Cookie] ' + JSON.stringify(req.headers.cookie))
  if (fs.existsSync(url)) {
    if (url.endsWith('.ico')) {
      return outBinary(req, res, url)
    } else {
      return outText(req, res, url)
    }
  } else {
    res.writeHead(404)
      res.end('Not Found')
  }
})

const port = process.env.PORT || 80
server.listen(port, function() {
  console.log(
    'To view your app, open this link in your browser: http://{EXTERNAL-IP}:' +
      port
  )
})
